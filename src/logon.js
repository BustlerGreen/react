import React from "react";

export default function Logon() {


    const [cred, setCred] = React.useState({
        user: "",
        psw: ""
    });
    
    const onChangeText = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setCred(oldCred => {return {...oldCred, [name]: value }})
    };

    const Login = () => {
        const url = "http://localhost:5000/api/v1/Login";
        console.log(`go to the api ${url} user ${cred.user}, password ${cred.psw}`);
        fetch(url, {
          method: "POST",
          mode: "cors",
          headers: {"Content-Type": "application/json"},
          body: JSON.stringify({login: cred.user, psw: cred.psw})
        })
        .then(res => res.json())
        .then(res => {
          console.log(res)
        })
        .catch(err => console.log(err))
    };

    return(
        <div>
            <p horizontal-align="center">identify yourself</p>
            <label horizontal-align='center' color="white" >Login</label>
            <br/>            
            <input type="text" name="user" value={cred.user} required min-length="4" max-length="56"onChange={onChangeText} />
            <br/>
            <label horizontal-align="center" >Password</label>
            <br/>
            <input type="password" name="psw" value={cred.psw} required min-length="8" max-length="12" onChange={onChangeText} />
            <br/>
            <div margin="10px">
                <button self-align="left" onClick={Login}>Login</button>
                <button self-align="right" onClick={()=>console.log("nothing to do with it")}>Cancel</button>
            </div>
        </div>

    )
    
}
