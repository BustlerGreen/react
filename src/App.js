import logo from './logo.svg';
import './App.css';
import Logon from './logon';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Logon />
        
      </header>
    </div>
  );
}

export default App;
